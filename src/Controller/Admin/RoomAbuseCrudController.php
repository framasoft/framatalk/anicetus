<?php

namespace App\Controller\Admin;

use App\Admin\Field\InetField;
use App\Entity\RoomAbuse;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;
use Symfony\Component\Translation\TranslatableMessage;

class RoomAbuseCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return RoomAbuse::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular(new TranslatableMessage('admin.crud.abuse.singular'))
            ->setEntityLabelInPlural(new TranslatableMessage('admin.crud.abuse.plural'))
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('roomName')->setLabel(new TranslatableMessage('admin.crud.abuse.roomName')),
            InetField::new('ip')->setLabel(new TranslatableMessage('admin.crud.abuse.ip')),
            UrlField::new('redirectUrl')->setLabel(new TranslatableMessage('admin.crud.abuse.redirectUrl')),
            TextEditorField::new('note')->setLabel(new TranslatableMessage('admin.crud.abuse.note')),
        ];
    }
}
