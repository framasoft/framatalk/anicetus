<?php

namespace App\Controller\Admin;

use App\Entity\RoomSession;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Translation\TranslatableMessage;

class RoomSessionCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return RoomSession::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular(new TranslatableMessage('admin.crud.room_session.singular'))
            ->setEntityLabelInPlural(new TranslatableMessage('admin.crud.room_session.plural'))
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('roomName')->setLabel(new TranslatableMessage('admin.crud.room_session.room_name')),
            AssociationField::new('creator')->setLabel(new TranslatableMessage('admin.crud.room_session.creator')),
            TextField::new('ip')->setLabel(new TranslatableMessage('admin.crud.room_session.ip')),
            TextField::new('userAgent')->setLabel(new TranslatableMessage('admin.crud.room_session.user_agent')),
            BooleanField::new('moderated')->setLabel(new TranslatableMessage('admin.crud.room_session.moderated')),
            DateTimeField::new('createdAt')->hideOnForm()->setLabel(new TranslatableMessage('admin.crud.room_session.created_at')),
            DateTimeField::new('updatedAt')->hideOnForm()->setLabel(new TranslatableMessage('admin.crud.room_session.updated_at')),
        ];
    }
}
