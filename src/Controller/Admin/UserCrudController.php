<?php

namespace App\Controller\Admin;

use App\Admin\Filter\RoleFilter;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Translation\TranslatableMessage;

class UserCrudController extends AbstractCrudController
{
    public function __construct(private readonly EntityManagerInterface $entityManager, private readonly AdminUrlGenerator $adminUrlGenerator, private readonly LoggerInterface $logger)
    {
    }

    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular(new TranslatableMessage('admin.crud.user.singular'))
            ->setEntityLabelInPlural(new TranslatableMessage('admin.crud.user.plural'))
            ->setDefaultSort(['updatedAt' => 'DESC'])
            ->setSearchFields(['userName', 'email'])
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('userName')
                ->setRequired(true)
                ->setLabel(new TranslatableMessage('admin.crud.user.username')),
            EmailField::new('email')->setLabel(new TranslatableMessage('admin.crud.user.email')),
            UrlField::new('avatar')->hideOnIndex()->setLabel(new TranslatableMessage('admin.crud.user.avatar')),
            ChoiceField::new('roles')
                ->allowMultipleChoices()
                ->renderAsBadges()
                ->renderExpanded()
                ->setChoices([
                    'Admin' => 'ROLE_ADMIN',
                ])
                ->setSortable(false)
                ->setLabel(new TranslatableMessage('admin.crud.user.roles')),
            BooleanField::new('isVerified')->setLabel(new TranslatableMessage('admin.crud.user.is_verified')),
            BooleanField::new('isSuspended')->setLabel(new TranslatableMessage('admin.crud.user.is_suspended')),
            DateTimeField::new('createdAt')->hideOnForm()->setLabel(new TranslatableMessage('admin.crud.user.created_at')),
            DateTimeField::new('updatedAt')->hideOnForm()->setLabel(new TranslatableMessage('admin.crud.user.updated_at')),
        ];
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('isVerified')
            ->add('isSuspended')
            ->add(RoleFilter::new('roles'))
        ;
    }

    public function configureActions(Actions $actions): Actions
    {
        $verifyUser = Action::new('verify', new TranslatableMessage('admin.action.user.verify'), 'fas fa-check')
            ->displayIf(static function ($entity) {
                if ($entity instanceof User) {
                    return !$entity->isVerified();
                }

                return false;
            })
            ->linkToCrudAction('verifyUser')
        ;

        $suspendUser = Action::new('suspend', new TranslatableMessage('admin.action.user.suspend'), 'fas fa-ban')
            ->displayIf(static function ($entity) {
                if ($entity instanceof User) {
                    return !$entity->isSuspended();
                }

                return false;
            })
            ->linkToCrudAction('suspendUser')
        ;

        $unSuspendUser = Action::new('unsuspend', new TranslatableMessage('admin.action.user.unsuspend'), 'fas fa-check')
            ->displayIf(static function ($entity) {
                if ($entity instanceof User) {
                    return $entity->isSuspended();
                }

                return false;
            })
            ->linkToCrudAction('unSuspendUser')
        ;

        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_INDEX, $verifyUser)
            ->add(Crud::PAGE_INDEX, $suspendUser)
            ->add(Crud::PAGE_INDEX, $unSuspendUser)
        ;
    }

    public function suspendUser(AdminContext $context): RedirectResponse
    {
        /** @var User $user */
        $user = $context->getEntity()->getInstance();

        $user->setIsSuspended(true);
        $user->setSuspendedAt(new \DateTimeImmutable());

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $this->logger->info('Admin user '.$context->getUser()->getUserIdentifier().' suspended user '.$user->getUserIdentifier());

        return $this->redirect($this->indexUrl());
    }

    public function unSuspendUser(AdminContext $context): RedirectResponse
    {
        /** @var User $user */
        $user = $context->getEntity()->getInstance();

        $user->setIsSuspended(false);
        $user->setSuspendedAt(null);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $this->logger->info('Admin user '.$context->getUser()->getUserIdentifier().' unsuspended user '.$user->getUserIdentifier());

        return $this->redirect($this->indexUrl());
    }

    public function verifyUser(AdminContext $context): RedirectResponse
    {
        /** @var User $user */
        $user = $context->getEntity()->getInstance();

        $user->setIsVerified(true);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $this->logger->info('Admin user '.$context->getUser()->getUserIdentifier().' marked user '.$user->getUserIdentifier().' as verified');

        return $this->redirect($this->indexUrl());
    }

    private function indexUrl(): string
    {
        return $this->adminUrlGenerator->setController(self::class)->setAction(Action::INDEX)->generateUrl();
    }
}
