<?php

namespace App\Controller\Admin;

use App\Entity\RoomAbuse;
use App\Entity\RoomSession;
use App\Entity\User;
use App\Repository\RoomSessionRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Translation\TranslatableMessage;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

class DashboardController extends AbstractDashboardController
{
    public function __construct(private readonly ChartBuilderInterface $chartBuilder, private readonly RoomSessionRepository $roomSessionRepository)
    {
    }

    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig', [
            'week_chart' => $this->produceStatsGraph($this->roomSessionRepository->getLastWeekRoomSessionStats(), 'per day last week'),
            'month_chart' => $this->produceStatsGraph($this->roomSessionRepository->getLastMonthRoomSessionStats(), 'per day last month'),
            'year_chart' => $this->produceStatsGraph($this->roomSessionRepository->getLastYearRoomSessionStats(), 'per month last year'),
        ]);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Anicetus');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard(new TranslatableMessage('admin.sidebar.dashboard'), 'fa fa-home');
        yield MenuItem::linkToCrud(new TranslatableMessage('admin.sidebar.users'), 'fas fa-user', User::class);
        yield MenuItem::linkToCrud(new TranslatableMessage('admin.sidebar.room_sessions'), 'fas fa-video', RoomSession::class);
        yield MenuItem::linkToCrud(new TranslatableMessage('admin.sidebar.abuses'), 'fas fa-flag', RoomAbuse::class);
        yield MenuItem::linkToUrl(new TranslatableMessage('admin.sidebar.back_to_user'), 'fas fa-user', '/');
    }

    public function configureUserMenu(UserInterface $user): UserMenu
    {
        /** @var User $user */
        return parent::configureUserMenu($user)
            ->setGravatarEmail($user->getEmail());
    }

    public function configureAssets(): Assets
    {
        return parent::configureAssets()
            ->addAssetMapperEntry('app');
    }

    private function produceStatsGraph(array $stats, string $label): ?Chart
    {
        if ($stats) {
            $chart = $this->chartBuilder->createChart(Chart::TYPE_LINE);
            $data = array_map(fn ($arr) => $arr['count'], $stats);

            $chart->setData([
                'labels' => array_map(fn ($arr) => $arr['date'], $stats),
                'datasets' => [
                    [
                        'label' => 'Room creations '.$label,
                        'backgroundColor' => 'rgb(255, 99, 132)',
                        'borderColor' => 'rgb(255, 99, 132)',
                        'data' => $data,
                    ],
                ],
            ]);

            $chart->setOptions([
                'scales' => [
                    'y' => [
                        'suggestedMin' => 0,
                        'suggestedMax' => max($data),
                    ],
                ],
            ]);

            return $chart;
        }

        return null;
    }
}
