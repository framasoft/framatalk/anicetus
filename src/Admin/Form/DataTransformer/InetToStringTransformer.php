<?php

namespace App\Admin\Form\DataTransformer;

use PhpIP\IP;
use PhpIP\IPBlock;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Transforms between an IP or IPBlock object and an inet string.
 *
 * @implements DataTransformerInterface<IP|IPBlock, string>
 */
class InetToStringTransformer implements DataTransformerInterface
{
    /**
     * Transforms a IP or IPBlock object into a string.
     *
     * @param IP|IPBlock $value A Uuid object
     *
     * @throws TransformationFailedException If the given value is not a Uuid object
     */
    public function transform(mixed $value): ?string
    {
        if (null === $value) {
            return null;
        }

        if (!$value instanceof IP && !$value instanceof IPBlock) {
            throw new TransformationFailedException('Expected an IP or an IPBlock.');
        }

        return (string) $value;
    }

    /**
     * Transforms a inet string into an IP or IPBlock object.
     *
     * @param string $value An inet string
     *
     * @throws TransformationFailedException If the given value is not a string,
     *                                       or could not be transformed
     */
    public function reverseTransform(mixed $value): IP|IPBlock|null
    {
        if (null === $value || '' === $value) {
            return null;
        }

        if (!\is_string($value)) {
            throw new TransformationFailedException('Expected a string.');
        }

        try {
            return str_contains($value, '/') ? IPBlock::create($value) : IP::create($value);
        } catch (\InvalidArgumentException $e) {
            throw new TransformationFailedException(sprintf('The value "%s" is not a valid IP or IPBlock.', $value), $e->getCode(), $e);
        }
    }
}
