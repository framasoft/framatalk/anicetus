<?php

namespace App\Entity;

use App\Entity\Traits\TimeStampableTrait;
use App\Repository\RoomAbuseRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use PhpIP\IP;
use PhpIP\IPBlock;
use SunChaser\Doctrine\PgSql\InetType;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: RoomAbuseRepository::class)]
class RoomAbuse
{
    use TimeStampableTrait;
    public const TYPE_CSAM = 'csam';
    public const TYPE_ILLEGAL = 'illegal';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'roomAbuses')]
    #[ORM\JoinColumn(nullable: true)]
    private ?RoomSession $roomSession = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $abuseType = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Assert\Url]
    private ?string $redirectUrl = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $note = null;

    #[ORM\Column(length: 255, unique: true, nullable: true)]
    private ?string $roomName = null;

    #[ORM\Column(type: InetType::NAME, length: 255, nullable: true)]
    #[Assert\Length(max: 255)]
    private IP|IPBlock|null $ip = null;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoomSession(): ?RoomSession
    {
        return $this->roomSession;
    }

    public function setRoomSession(?RoomSession $roomId): static
    {
        $this->roomSession = $roomId;

        return $this;
    }

    public function getAbuseType(): ?string
    {
        return $this->abuseType;
    }

    public function setAbuseType(?string $abuseType): static
    {
        $this->abuseType = $abuseType;

        return $this;
    }

    public function getRedirectUrl(): ?string
    {
        if ($this->redirectUrl) {
            return $this->redirectUrl;
        }

        return match ($this->abuseType) {
            self::TYPE_CSAM => 'https://blocage.framasoft.org',
            self::TYPE_ILLEGAL => 'https://blocage.framasoft.org',
            default => 'https://blocage.framasoft.org',
        };
    }

    public function setRedirectUrl(?string $redirectUrl): static
    {
        $this->redirectUrl = $redirectUrl;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): static
    {
        $this->note = $note;

        return $this;
    }

    public function getRoomName(): ?string
    {
        return $this->roomName;
    }

    public function setRoomName(string $roomName): static
    {
        $this->roomName = $roomName;

        return $this;
    }

    public function getIp(): IP|IPBlock|null
    {
        return $this->ip;
    }

    public function setIp(IP|IPBlock|null $ip): RoomAbuse
    {
        $this->ip = $ip;

        return $this;
    }
}
