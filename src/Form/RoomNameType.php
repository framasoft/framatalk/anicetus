<?php

namespace App\Form;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatableMessage;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RoomNameType extends AbstractType
{
    public function __construct(private readonly ParameterBagInterface $params)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $numberOfChars = $this->params->get('app.service.room.suggested_name.nb_characters');
        $randomRoomName = substr(str_shuffle(str_repeat('0123456789abcdefghijklmnopqrstuvwxyz', $numberOfChars)), 0, $numberOfChars);

        $builder
            ->add('roomName', TextType::class, [
                'label' => new TranslatableMessage('room.index.form.roomName.label'),
                'data' => $randomRoomName,
                'constraints' => [
                    new NotBlank([
                        'message' => 'room.index.form.roomName.constraints.blank',
                    ]),
                    new Length([
                        'max' => $this->params->get('app.service.room.name.max_characters'),
                        'min' => $this->params->get('app.service.room.name.min_characters'),
                        'maxMessage' => 'room.index.form.roomName.constraints.maxLength',
                        'minMessage' => 'room.index.form.roomName.constraints.minLength',
                    ]),
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => new TranslatableMessage('room.index.regular_meeting.create_btn'),
                'attr' => [
                    'class' => 'btn btn-lg btn-primary w-100',
                    'disabled' => $options['userSuspended'],
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'userSuspended' => false,
        ]);
    }
}
