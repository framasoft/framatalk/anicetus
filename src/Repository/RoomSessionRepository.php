<?php

namespace App\Repository;

use App\Entity\RoomSession;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RoomSession>
 *
 * @method RoomSession|null find($id, $lockMode = null, $lockVersion = null)
 * @method RoomSession|null findOneBy(array $criteria, array $orderBy = null)
 * @method RoomSession[]    findAll()
 * @method RoomSession[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoomSessionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RoomSession::class);
    }

    public function getLastWeekRoomSessionStats()
    {
        $date = date('Y-m-d h:i:s', strtotime('-7 days'));

        $qb = $this->createQueryBuilder('rs');

        return $qb->select([$qb->expr()->count('rs.id').' AS count', 'DATE(rs.createdAt) AS date'])
            ->groupBy('date')
            ->where($qb->expr()->gt('rs.createdAt', ':lastWeek'))
            ->orderBy('date', 'asc')
            ->setParameter(':lastWeek', $date)
            ->getQuery()
            ->execute();
    }

    public function getLastMonthRoomSessionStats()
    {
        $date = date('Y-m-d h:i:s', strtotime('-30 days'));

        $qb = $this->createQueryBuilder('rs');

        return $qb->select([$qb->expr()->count('rs.id').' AS count', 'DATE(rs.createdAt) AS date'])
            ->groupBy('date')
            ->where($qb->expr()->gt('rs.createdAt', ':lastMonth'))
            ->orderBy('date', 'asc')
            ->setParameter(':lastMonth', $date)
            ->getQuery()
            ->execute();
    }

    public function getLastYearRoomSessionStats()
    {
        $date = date('Y-m-d h:i:s', strtotime('-1 years'));

        $qb = $this->createQueryBuilder('rs');

        return $qb->select([$qb->expr()->count('rs.id').' AS count', "DATE_TRUNC('month', rs.createdAt) AS date"])
            ->groupBy('date')
            ->where($qb->expr()->gt('rs.createdAt', ':lastYear'))
            ->orderBy('date', 'asc')
            ->setParameter(':lastYear', $date)
            ->getQuery()
            ->execute();
    }

    //    /**
    //     * @return RoomSession[] Returns an array of Room objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('r')
    //            ->andWhere('r.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('r.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?RoomSession
    //    {
    //        return $this->createQueryBuilder('r')
    //            ->andWhere('r.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
