<?php

namespace App\Tests\Controller;

use App\Factory\UserFactory;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RoomControllerTest extends WebTestCase
{
    public function testWithoutLogin()
    {
        $client = static::createClient();
        $client->request('GET', '/');
        $this->assertResponseStatusCodeSame(302);
        $this->assertResponseRedirects('/login');

        $client->request('GET', '/room/roomName/auth');
        $this->assertResponseStatusCodeSame(302);
        $this->assertResponseRedirects('/login');
    }

    public function testIndex()
    {
        $client = static::createClient();
        $user = UserFactory::createOne();
        $client->loginUser($user->_real());
        $crawler = $client->request('GET', '/');
        $this->assertResponseStatusCodeSame(200);
        $submitButton = $crawler->selectButton('room_name[submit]');
        $form = $submitButton->form();
        $form['room_name[roomName]'] = 'someroomname';
        $client->submit($form);
        $this->assertResponseStatusCodeSame(302);
        $this->assertResponseRedirects('/room/someroomname/auth');
    }

    public function testAuth()
    {
        $client = static::createClient();
        $user = UserFactory::createOne();
        $client->loginUser($user->_real());
        $client->request('GET', '/room/someroomname/auth');
        $this->assertResponseStatusCodeSame(302);
        $response = $client->getResponse();
        $redirectURL = $response->headers->get('Location');
        $jitsiDomain = $this->getContainer()->getParameter('app.jitsi.domain');
        $this->assertStringStartsWith("https://$jitsiDomain/someroomname?jwt=", $redirectURL);

        parse_str(parse_url($redirectURL, PHP_URL_QUERY), $queryArgs);
        $jwt = $queryArgs['jwt'];
        [$header, $payload, $signature] = explode('.', $jwt);
        $jsonToken = self::urlsafeB64Decode($payload);
        $arrayToken = json_decode($jsonToken, true);
        $userData = $arrayToken['context']['user'];
        $this->assertEquals($user->getId(), $userData['id']);
        $this->assertEquals($user->getUserName(), $userData['name']);
        $this->assertEquals('someroomname', $arrayToken['room']);
    }

    private static function urlsafeB64Decode(string $input): string
    {
        return \base64_decode(self::convertBase64UrlToBase64($input));
    }

    private static function convertBase64UrlToBase64(string $input): string
    {
        $remainder = \strlen($input) % 4;
        if ($remainder) {
            $padlen = 4 - $remainder;
            $input .= \str_repeat('=', $padlen);
        }

        return \strtr($input, '-_', '+/');
    }
}
