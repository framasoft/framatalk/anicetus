# Anicetus

> Anicetus [is a] the immortal son of Heracles, the greatest of the Greek heroes, and Hebe, the goddess of youth. [He was] likely responsible for the protection and fortification of towns and citadels and may has been regarded as gatekeeper of Olympus.

https://en.wikipedia.org/wiki/Alexiares_and_Anicetus

Anicetus is an authentification system to put in front of a Jitsi Meet server. It was developed for Framasoft's own needs with the [Framatalk](https://framatalk.org) service, which was briefly suspended due to serious abuses.

Anicetus simply makes registration required for all Jitsi room creations (for people organising meetings). People invited to simply join a room do not need an account.

Administrators can block room creations by:
- Room name
- User IP Address

They can also customize the redirection URL.

Finally, Anicetus also provides a feature similar to [moderated.jitsi.net](https://moderated.jitsi.net) to allow to "book" special room URLs to make sure you're the only moderator on the given room.

## Dependencies

```bash
apt install php-fpm php-intl php-xml php-gmp php-curl php-postgresql postgresql
```

Install [composer](https://getcomposer.org/download/) on your system.

Create a PostgreSQL user and a database for anicetus.

## Installation

```bash
cd /var/www
git clone https://framagit.org/framasoft/framatalk/anicetus.git
cd anicetus
SYMFONY_ENV=prod composer install --no-dev --optimize-autoloader
git submodule init
git submodule update
cp .env .env.local
SYMFONY_ENV=prod composer dump-env prod
```

Edit `.env.local`.

```bash
php bin/console sass:build
php bin/console doctrine:migrations:migrate
chown www-data: -R .
cat <<EOF > /etc/systemd/system/anicetus-async.service
[Unit]
Description=Anicetus Async Service
After=network.target postgresql.service

[Service]
User=www-data
WorkingDirectory=/var/www/anicetus
ExecStart=/usr/bin/php8.2 bin/console messenger:consume async --time-limit=3600
KillMode=process
Restart=always
RestartSec=30

SyslogIdentifier=anicetus-async


[Install]
WantedBy=multi-user.target
EOF
systemctl daemon-reload
systemctl enable --now anicetus-async.service
```

## Jitsi Configuration

We make the assumption you already have a working Jitsi Meet setup.

The following docs may also be of help:
- https://jitsi.github.io/handbook/docs/devops-guide/secure-domain
- https://github.com/jitsi/lib-jitsi-meet/blob/master/doc/tokens.md
- https://www.redpill-linpro.com/techblog/2023/03/08/Jitsi-with-JWT-and-Moderated-Meetings.html
- https://meetrix.io/blog/webrtc/jitsi/meet/how-to-authenticate-users-to-Jitsi-meet-using-JWT-tokens.html

### Dependencies

You need to install the `jitsi-meet-tokens` package.

### Meet

Edit your Jitsi Meet `config.js` file like this:
```js
{
    // ...
    tokenAuthUrl: 'https://anicetus.your-jitsi-meet.org/room/{room}/auth'
    // ...
    anonymousdomain: 'guest.your-jitsi-meet.org'
}
```
Where the `anicetus` subdomain should be serving Anicetus, and `guest` the virtualhost anonymousdomain for guests users.

### Jicofo

Edit `/etc/jitsi/jicofo/jicofo.conf` to add a `authentication` section:
```
jicofo {

  authentication: {
    enabled: true
    type: "JWT"
    login-url: "your-jitsi-meet.org"
  }
}
```

### Prosody

Edit your lua config file (something like `/etc/prosody/conf.avail/your-jitsi-meet.org.cfg.lua`):

On the root level, add the two following lines:
```lua
asap_accepted_issuers = { "*" }
asap_accepted_audiences = { "*" }
```

Under your host configuration, set authentification to `token`, add an `app_id` and `app_secret`, set `allow_empty_token = true` and add `presence_identity` to `modules_enabled`.

It should then look something like this:
```lua
VirtualHost "your-jitsi-meet.org"
        -- ...
        --authentication = "anonymous"
        authentication = "token"
        -- Properties below are modified by jitsi-meet-tokens package config
        -- and authentication above is switched to "token"
        app_id="some-app-id"
        app_secret="some-very-secret-app-secret"
        allow_empty_token = true;
        -- ...
        modules_enabled = {
            -- ...
            "presence_identity"; -- Token context provides user metadata
        }
        -- ...
```

Then, add a virtual host for the guest login method:

```lua
VirtualHost "guest.your-jitsi-meet.org"
    authentication = "anonymous"
    c2s_require_encryption = false
```

Finally, add the `token_verification` module to the conference component:
```lua
Component "conference.your-jitsi-meet.org" "muc"
    restrict_room_creation = true
    -- ...
    modules_enabled = {
        -- ...
        "token_verification";
    }
```

Make sure to set `APP_JITSI_APP_ID` and `APP_JITSI_APP_SECRET` in Anicetus environment configuration file matching the values set above, and you should be ready to go.

## License

Anicetus is licensed under the terms of the GNU Affero GPLv3. See [LICENSE](LICENSE) file.
